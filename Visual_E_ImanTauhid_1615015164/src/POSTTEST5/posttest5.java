package POSTTEST5;

import java.awt.Color;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author TauhidIman.S
 */
public class posttest5 extends javax.swing.JFrame {

    private DefaultTableModel model;
    private Connection con = koneksi.getConnection();
    private Statement stt;
    private ResultSet rss;
    
    public posttest5() {
        initComponents();
        this.setTitle("Post Test Visual //Created By TauhidIman.S E2016");
        this.setResizable(false);       
    }
    
    private void InitTable(){
        model = new DefaultTableModel();
        model.addColumn("Id");
        model.addColumn("ID_Gemscool");
        model.addColumn("Level");
        model.addColumn("Exp");
        model.addColumn("Tipe_Potion");
        model.addColumn("Jumlah_Potion");
        model.addColumn("Tersimpan");
        model.addColumn("Total_Exp");
        jTable1.setModel(model);
    }
    
    private void TampilData(){
        try{
            String sql = "SELECT * FROM atmexp";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()){
                Object[] o = new Object[8];
                o[0] = rss.getString("id");
                o[1] = rss.getString("id_gemscool");
                o[2] = rss.getString("level");
                o[3] = rss.getString("exp");
                o[4] = rss.getString("tipe_potion");
                o[5] = rss.getString("jumlah_potion");
                o[6] = rss.getString("tersimpan");
                o[7] = rss.getString("totalexp");
                model.addRow(o);
              }
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }   
    
    private void TambahData(String id_gemscool, int level, String exp ,int tipe_potion, int jumlah_potion, String tersimpan, int totalexp){
        try{
                String sql = "INSERT INTO atmexp VALUES (NULL,'"+id_gemscool+"',"+level+","+exp+","+tipe_potion+","+jumlah_potion+",'"+tersimpan+"',"+totalexp+")";
                stt = con.createStatement();
                stt.executeUpdate(sql);
                model.addRow(new Object[]{id_gemscool,level,exp,tipe_potion,jumlah_potion,tersimpan,totalexp});
                JOptionPane.showMessageDialog(null, "Data Berhasil Ditambahkan");
            }           
        catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    public boolean UbahData(String id,String id_gemscool, int level, String exp ,int tipe_potion, int jumlah_potion, String tersimpan, int totalexp){
        try{
            String sql = "Update atmexp set id_gemscool='"+id_gemscool+"', level='"+level
                    +"', exp="+exp+" , tipe_potion="+tipe_potion+",jumlah_potion="+jumlah_potion+",tersimpan='"+tersimpan+"',totalexp="+totalexp+"  WHERE id="+id+";";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            return true;
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return false;
        }
    }  
    
    public boolean HapusData(String id){
        try{
            String sql = "DELETE FROM atmexp WHERE id="+id+";";
            stt = con.createStatement();
            stt.executeUpdate(sql);
            return true;
        }
        catch(SQLException e){
            System.out.println(e.getMessage());
            return false;
        }
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        jMenu3 = new javax.swing.JMenu();
        jLabel12 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jPanelatas = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPaneltengah = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        id_gemscooljfield = new javax.swing.JTextField();
        jsliderexp = new javax.swing.JSlider();
        jslidertipe = new javax.swing.JSlider();
        jradiostorage = new javax.swing.JRadioButton();
        jradioinventory = new javax.swing.JRadioButton();
        jspinnerjumlahpotion = new javax.swing.JSpinner();
        jspinnerlevel = new javax.swing.JSpinner();
        expkarakter = new javax.swing.JTextField();
        simpan = new javax.swing.JButton();
        ubah = new javax.swing.JButton();
        hapus = new javax.swing.JButton();
        exit = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        green = new javax.swing.JMenu();
        black = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        blue = new javax.swing.JMenuItem();
        red = new javax.swing.JMenuItem();
        gray = new javax.swing.JMenuItem();
        jMenu11 = new javax.swing.JMenu();
        jMenuItem21 = new javax.swing.JMenuItem();
        purple = new javax.swing.JMenuItem();
        blue2 = new javax.swing.JMenuItem();
        jMenuItem24 = new javax.swing.JMenuItem();
        jMenuItem25 = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();

        jMenu3.setText("jMenu3");

        jLabel12.setText("Large");

        jButton3.setText("jButton3");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 51, 0));
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });

        jPanelatas.setBackground(new java.awt.Color(153, 51, 0));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tw Cen MT Condensed", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("GEMSCOOL - GENERATOR 100% WORK");

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Tw Cen MT Condensed", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("PREMIUM EXP POTION ATLANTICA ONLINE");

        javax.swing.GroupLayout jPanelatasLayout = new javax.swing.GroupLayout(jPanelatas);
        jPanelatas.setLayout(jPanelatasLayout);
        jPanelatasLayout.setHorizontalGroup(
            jPanelatasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelatasLayout.createSequentialGroup()
                .addGap(295, 295, 295)
                .addGroup(jPanelatasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelatasLayout.setVerticalGroup(
            jPanelatasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelatasLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47))
        );

        jPaneltengah.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel2.setText("ID Gemscool");

        jLabel5.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel5.setText("Tipe Exp Potion :");

        jLabel6.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel6.setText("Simpan Exp Potion :");

        jLabel7.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel7.setText("Pcs");

        jLabel8.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel8.setText("Jumlah Exp Potion");

        jLabel9.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel9.setText("S");

        jLabel10.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel10.setText("M");

        jLabel11.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel11.setText("L");

        jLabel20.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel20.setText("Level :");

        jLabel21.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel21.setText("EXP Karakter Sekarang");

        jLabel22.setText("%");

        jLabel23.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel23.setText("Note Tipe EXP :");

        jLabel24.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel24.setText("S = 25K EXP");

        jLabel25.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel25.setText("L  = 75K EXP");

        jLabel26.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel26.setText("M = 50K EXP");

        jLabel27.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel27.setText("XL = 100K EXP");

        jLabel28.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jLabel28.setText("XL");

        id_gemscooljfield.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        id_gemscooljfield.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                id_gemscooljfieldActionPerformed(evt);
            }
        });

        jsliderexp.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jsliderexpStateChanged(evt);
            }
        });

        jslidertipe.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jslidertipe.setMaximum(4);
        jslidertipe.setMinimum(1);

        buttonGroup1.add(jradiostorage);
        jradiostorage.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jradiostorage.setText("Storage");

        buttonGroup1.add(jradioinventory);
        jradioinventory.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        jradioinventory.setText("Inventory");

        jspinnerjumlahpotion.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N

        expkarakter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expkarakterActionPerformed(evt);
            }
        });

        simpan.setBackground(new java.awt.Color(0, 204, 255));
        simpan.setFont(new java.awt.Font("Arial Rounded MT Bold", 0, 13)); // NOI18N
        simpan.setText("Save");
        simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                simpanActionPerformed(evt);
            }
        });

        ubah.setBackground(new java.awt.Color(0, 204, 0));
        ubah.setText("Edit");
        ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ubahActionPerformed(evt);
            }
        });

        hapus.setBackground(new java.awt.Color(255, 153, 153));
        hapus.setText("Delete");
        hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hapusActionPerformed(evt);
            }
        });

        exit.setBackground(new java.awt.Color(0, 0, 0));
        exit.setFont(new java.awt.Font("OCR A Extended", 0, 13)); // NOI18N
        exit.setForeground(new java.awt.Color(255, 255, 255));
        exit.setText("Exit");
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPaneltengahLayout = new javax.swing.GroupLayout(jPaneltengah);
        jPaneltengah.setLayout(jPaneltengahLayout);
        jPaneltengahLayout.setHorizontalGroup(
            jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPaneltengahLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPaneltengahLayout.createSequentialGroup()
                        .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addGap(71, 71, 71)
                                .addComponent(jLabel2))
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel18)
                        .addGap(9, 9, 9))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPaneltengahLayout.createSequentialGroup()
                        .addComponent(simpan, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(hapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(exit, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPaneltengahLayout.createSequentialGroup()
                        .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel21)
                            .addComponent(jslidertipe, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addComponent(jsliderexp, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(expkarakter, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel22))
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addComponent(id_gemscooljfield, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel20)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jspinnerlevel, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPaneltengahLayout.createSequentialGroup()
                                        .addComponent(jradioinventory)
                                        .addGap(46, 46, 46)
                                        .addComponent(jradiostorage))
                                    .addGroup(jPaneltengahLayout.createSequentialGroup()
                                        .addGap(60, 60, 60)
                                        .addComponent(jLabel6)
                                        .addGap(28, 28, 28)))
                                .addGap(58, 58, 58)
                                .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel23)
                                    .addComponent(jLabel25)
                                    .addComponent(jLabel24)
                                    .addComponent(jLabel26)
                                    .addComponent(jLabel27)))
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(jspinnerjumlahpotion, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel7))
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(119, 119, 119)
                                .addComponent(jLabel10)
                                .addGap(126, 126, 126)
                                .addComponent(jLabel11)
                                .addGap(108, 108, 108)
                                .addComponent(jLabel28)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(290, 290, 290))
        );
        jPaneltengahLayout.setVerticalGroup(
            jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPaneltengahLayout.createSequentialGroup()
                .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPaneltengahLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(jspinnerlevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(id_gemscooljfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel21)
                        .addGap(9, 9, 9)
                        .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jsliderexp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel22)
                                .addComponent(expkarakter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(6, 6, 6)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jslidertipe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel11)
                                .addComponent(jLabel28)
                                .addComponent(jLabel10)))
                        .addGap(18, 18, 18)
                        .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(jspinnerjumlahpotion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7))
                                .addGap(48, 48, 48)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jradiostorage)
                                    .addComponent(jradioinventory)))
                            .addGroup(jPaneltengahLayout.createSequentialGroup()
                                .addComponent(jLabel23)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel24)
                                .addGap(8, 8, 8)
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel27))))
                    .addGroup(jPaneltengahLayout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addComponent(jLabel18)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPaneltengahLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(exit, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(simpan, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(hapus, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40))
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable1);

        jMenu2.setText("Theme");

        green.setText("Change Title Background Color");
        green.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greenActionPerformed(evt);
            }
        });

        black.setText("Black");
        black.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blackActionPerformed(evt);
            }
        });
        green.add(black);

        jMenuItem17.setText("Green");
        green.add(jMenuItem17);

        blue.setText("Blue");
        blue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blueActionPerformed(evt);
            }
        });
        green.add(blue);

        red.setText("Red");
        red.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redActionPerformed(evt);
            }
        });
        green.add(red);

        gray.setText("Gray");
        gray.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grayActionPerformed(evt);
            }
        });
        green.add(gray);

        jMenu2.add(green);

        jMenu11.setText("Change Body Background Color");

        jMenuItem21.setText("Gray");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem21);

        purple.setText("Orange");
        purple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purpleActionPerformed(evt);
            }
        });
        jMenu11.add(purple);

        blue2.setText("Blue");
        blue2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blue2ActionPerformed(evt);
            }
        });
        jMenu11.add(blue2);

        jMenuItem24.setText("White");
        jMenuItem24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem24ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem24);

        jMenuItem25.setText("Cyan");
        jMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem25ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem25);

        jMenu2.add(jMenu11);

        jMenuBar1.add(jMenu2);

        jMenu9.setText("About Us");

        jMenuItem2.setText("Check for Update");
        jMenu9.add(jMenuItem2);

        jMenuItem3.setText("About Posttest");
        jMenu9.add(jMenuItem3);

        jMenuBar1.add(jMenu9);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelatas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPaneltengah, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(jPanelatas, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPaneltengah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem24ActionPerformed
      jPaneltengah.setBackground(Color.white);
    }//GEN-LAST:event_jMenuItem24ActionPerformed

    private void greenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greenActionPerformed
       jPanelatas.setBackground(Color.green); 
    }//GEN-LAST:event_greenActionPerformed

    private void blackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blackActionPerformed
       jPanelatas.setBackground(Color.black);
    }//GEN-LAST:event_blackActionPerformed

    private void blueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blueActionPerformed
        jPanelatas.setBackground(Color.blue);
    }//GEN-LAST:event_blueActionPerformed

    private void redActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redActionPerformed
        jPanelatas.setBackground(Color.red);        // TODO add your handling code here:
    }//GEN-LAST:event_redActionPerformed

    private void grayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grayActionPerformed
        jPanelatas.setBackground(Color.gray);        // TODO add your handling code here:
    }//GEN-LAST:event_grayActionPerformed

    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed
        jPaneltengah.setBackground(Color.gray);        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem21ActionPerformed

    private void blue2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blue2ActionPerformed
        jPaneltengah.setBackground(Color.blue);        // TODO add your handling code here:
    }//GEN-LAST:event_blue2ActionPerformed

    private void purpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purpleActionPerformed
        jPaneltengah.setBackground(Color.orange);        // TODO add your handling code here:
    }//GEN-LAST:event_purpleActionPerformed

    private void jMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem25ActionPerformed
        jPaneltengah.setBackground(Color.cyan);        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem25ActionPerformed

    private void expkarakterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expkarakterActionPerformed

    }//GEN-LAST:event_expkarakterActionPerformed

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        System.exit(WIDTH);
    }//GEN-LAST:event_exitActionPerformed

    private void jsliderexpStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jsliderexpStateChanged
        String exp = String.valueOf(jsliderexp.getValue());
        expkarakter.setText(exp);
    }//GEN-LAST:event_jsliderexpStateChanged

    private void simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_simpanActionPerformed

        //ID
        String id_gemscool = id_gemscooljfield.getText();

        //SPINNER LEVEL KARAKTER
        Object spinner =jspinnerlevel.getValue();
        int level = Integer.parseInt(spinner.toString());

        //SPINNER JUMLAH EXP POTION
        Object spinner2 =jspinnerjumlahpotion.getValue();
        int jumlah_potion =Integer.parseInt(spinner2.toString());

        //SIMPAN EXP POTION
        String tersimpan = "";
        if(jradioinventory.isSelected()){
            tersimpan = jradioinventory.getText();
        }
        else if (jradiostorage.isSelected()){
            tersimpan = jradiostorage.getText();
        }

        String exp = expkarakter.getText();

        //TIPE EXP POTION
        String tipepotion = null;
        int tipe_potion = jslidertipe.getValue();
        int besarexp = 0;
        if(tipe_potion == 1){
            tipepotion = "Small";
            besarexp = 25000;
        }
        else if(tipe_potion == 2){
            tipepotion = "Medium";
            besarexp = 50000;
        }
        else if(tipe_potion == 3){
            tipepotion = "Large";
            besarexp = 75000;
        }
        else if(tipe_potion == 4){
            tipepotion = "Extra Large";
            besarexp = 100000;
        }

        int totalexp = besarexp*jumlah_potion;
         
        TambahData(id_gemscool,level,exp,tipe_potion,jumlah_potion,tersimpan,totalexp);
        InitTable();
        TampilData();
    }//GEN-LAST:event_simpanActionPerformed

    private void id_gemscooljfieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_id_gemscooljfieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_id_gemscooljfieldActionPerformed

    private void hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hapusActionPerformed
        int baris = jTable1.getSelectedRow();
        String id = jTable1.getValueAt(baris, 0).toString();
        if(HapusData(id)){
            JOptionPane.showMessageDialog(null, "Berhasil Hapus Data");
        }   
        else{
            JOptionPane.showConfirmDialog(null, "Gagal Hapus Data");
        }
        InitTable();
        TampilData(); 
    }//GEN-LAST:event_hapusActionPerformed

    private void ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ubahActionPerformed
        int baris = jTable1.getSelectedRow();
        String id = jTable1.getValueAt(baris, 0).toString();
        //ID
        String id_gemscool = id_gemscooljfield.getText();

        //SPINNER LEVEL KARAKTER
        Object spinner =jspinnerlevel.getValue();
        int level = Integer.parseInt(spinner.toString());

        //SPINNER JUMLAH EXP POTION
        Object spinner2 =jspinnerjumlahpotion.getValue();
        int jumlah_potion =Integer.parseInt(spinner2.toString());

        //SIMPAN EXP POTION
        String tersimpan = "";
        if(jradioinventory.isSelected()){
            tersimpan = jradioinventory.getText();
        }
        else if (jradiostorage.isSelected()){
            tersimpan = jradiostorage.getText();
        }

        String exp = expkarakter.getText();

        //TIPE EXP POTION
        String tipepotion = null;
        int tipe_potion = jslidertipe.getValue();
        int besarexp = 0;
        if(tipe_potion == 1){
            tipepotion = "Small";
            besarexp = 25000;
        }
        else if(tipe_potion == 2){
            tipepotion = "Medium";
            besarexp = 50000;
        }
        else if(tipe_potion == 3){
            tipepotion = "Large";
            besarexp = 75000;
        }
        else if(tipe_potion == 4){
            tipepotion = "Extra Large";
            besarexp = 100000;
        }

        int totalexp = besarexp*jumlah_potion;
        if(UbahData(id,id_gemscool,level,exp,tipe_potion,jumlah_potion,tersimpan,totalexp)){
            JOptionPane.showMessageDialog(null, "Berhasil Ubah Data");
        }   
        else{
            JOptionPane.showConfirmDialog(null, "Gagal Ubah Data");
        }
        InitTable();
        TampilData();  
    }//GEN-LAST:event_ubahActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        InitTable();
        TampilData();
    }//GEN-LAST:event_formComponentShown

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int baris = jTable1.getSelectedRow();
        
        id_gemscooljfield.setText(jTable1.getValueAt(baris, 1).toString());
        
        String level = jTable1.getValueAt(baris,2).toString();
        jspinnerlevel.setValue(Integer.parseInt(level));
        
        String exp = jTable1.getValueAt(baris,3).toString();
        jsliderexp.setValue(Integer.parseInt(exp));
        
        String tipe_potion = jTable1.getValueAt(baris,4).toString();
        jslidertipe.setValue(Integer.parseInt(tipe_potion));
        
        String jumlah_potion = jTable1.getValueAt(baris,5).toString();
        jspinnerjumlahpotion.setValue(Integer.parseInt(jumlah_potion));
        
        
        String tersimpan = jTable1.getValueAt(baris,6).toString();
        if("Inventory".equals(tersimpan)){
            jradioinventory.setSelected(true);
        }
        else{
            jradiostorage.setSelected(true);
        }
    
    }//GEN-LAST:event_jTable1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(posttest5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(posttest5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(posttest5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(posttest5.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new posttest5().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem black;
    private javax.swing.JMenuItem blue;
    private javax.swing.JMenuItem blue2;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.JButton exit;
    private javax.swing.JTextField expkarakter;
    private javax.swing.JMenuItem gray;
    private javax.swing.JMenu green;
    private javax.swing.JButton hapus;
    private javax.swing.JTextField id_gemscooljfield;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem24;
    private javax.swing.JMenuItem jMenuItem25;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanelatas;
    private javax.swing.JPanel jPaneltengah;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JRadioButton jradioinventory;
    private javax.swing.JRadioButton jradiostorage;
    private javax.swing.JSlider jsliderexp;
    private javax.swing.JSlider jslidertipe;
    private javax.swing.JSpinner jspinnerjumlahpotion;
    private javax.swing.JSpinner jspinnerlevel;
    private javax.swing.JMenuItem purple;
    private javax.swing.JMenuItem red;
    private javax.swing.JButton simpan;
    private javax.swing.JButton ubah;
    // End of variables declaration//GEN-END:variables
}
