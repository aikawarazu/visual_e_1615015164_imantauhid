-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 20, 2018 at 03:16 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_visual`
--

-- --------------------------------------------------------

--
-- Table structure for table `atmexp`
--

CREATE TABLE `atmexp` (
  `id` int(10) NOT NULL,
  `id_gemscool` varchar(255) NOT NULL,
  `level` int(10) NOT NULL,
  `exp` int(10) NOT NULL,
  `tipe_potion` int(11) NOT NULL,
  `jumlah_potion` int(11) NOT NULL,
  `tersimpan` varchar(255) NOT NULL,
  `totalexp` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atmexp`
--

INSERT INTO `atmexp` (`id`, `id_gemscool`, `level`, `exp`, `tipe_potion`, `jumlah_potion`, `tersimpan`, `totalexp`) VALUES
(5, 'a', 2, 60, 3, 3, 'Inventory', 225000),
(6, 'zsar98', 57, 13, 4, 90, 'Storage', 9000000),
(7, 'Triscoid', 6, 63, 3, 3, 'Inventory', 225000),
(8, 'Mahoyo', 19, 17, 2, 8, 'Inventory', 400000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atmexp`
--
ALTER TABLE `atmexp`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atmexp`
--
ALTER TABLE `atmexp`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
